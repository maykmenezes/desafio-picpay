package com.maykmenezes.core.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

internal class HttpClient {

    fun getHttpClient(): OkHttpClient {
        val client = OkHttpClient.Builder()
            .connectTimeout(CONNECTION_TIMEOUT_VALUE, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT_VALUE, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT_VALUE, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        return client.build()
    }

    companion object {
        private const val READ_TIMEOUT_VALUE = 60L
        private const val WRITE_TIMEOUT_VALUE = 60L
        private const val CONNECTION_TIMEOUT_VALUE = 60L
    }
}