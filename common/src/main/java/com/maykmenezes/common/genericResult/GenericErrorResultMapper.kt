package com.maykmenezes.common.genericResult

import retrofit2.HttpException
import java.net.SocketTimeoutException

object GenericErrorResultMapper {

    fun <T> exceptionErrorToGenericErrorResult(exception: Exception): GenericResult<T> =
        when (exception) {
            is SocketTimeoutException -> {
                GenericResult.Error(GenericErrorResult.Timeout(exception))
            }
            is HttpException -> {
                getGenericErrorResultByHttpExceptionCode(exception.code(), exception)
            }
            else -> {
                GenericResult.Error(GenericErrorResult.Unknown(exception))
            }
        }

    private fun <T> getGenericErrorResultByHttpExceptionCode(
        code: Int,
        exception: Exception
    ): GenericResult<T> = when (code) {
        UNAUTHORIZED -> {
            GenericResult.Error(GenericErrorResult.Unauthorized(exception))
        }
        NOT_FOUND -> {
            GenericResult.Error(GenericErrorResult.NotFound(exception))
        }
        else -> {
            GenericResult.Error(GenericErrorResult.Unknown(exception))
        }
    }

    private const val UNAUTHORIZED = 401
    private const val NOT_FOUND = 404
}