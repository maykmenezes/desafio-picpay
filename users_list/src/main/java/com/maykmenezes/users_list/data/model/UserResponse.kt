package com.maykmenezes.users_list.data.model

import com.google.gson.annotations.SerializedName

internal data class UserResponse(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("img")
    val urlImage: String?,
    @SerializedName("username")
    val userName: String?
)