package com.maykmenezes.users_list.data.dataSource.local

import com.maykmenezes.core.database.usersList.UserDao
import com.maykmenezes.core.database.usersList.UserEntity

internal class UsersLocalDataSourceImpl(
    private val userDao: UserDao
) : UsersLocalDataSource {

    override suspend fun insertUser(user: UserEntity) = userDao.insertUser(user)
    override suspend fun fetchUsers(): List<UserEntity> = userDao.fetchUsers()
    override suspend fun deleteAllUsers() = userDao.deleteAllUsers()
}