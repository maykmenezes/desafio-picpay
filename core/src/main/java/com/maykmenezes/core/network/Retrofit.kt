package com.maykmenezes.core.network

import com.maykmenezes.core.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal class Retrofit {

    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(HttpClient().getHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}