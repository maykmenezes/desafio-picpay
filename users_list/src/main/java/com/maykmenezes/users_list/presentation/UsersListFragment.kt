package com.maykmenezes.users_list.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import com.maykmenezes.common.presentation.ViewState
import com.maykmenezes.helper.extensionFunction.gone
import com.maykmenezes.helper.extensionFunction.visible
import com.maykmenezes.helper.network.NetworkHelper
import com.maykmenezes.users_list.databinding.FragmentUsersListBinding
import com.maykmenezes.users_list.presentation.model.UserUiModel
import org.koin.androidx.viewmodel.ext.android.viewModel

internal class UsersListFragment : Fragment() {

    private val viewModel: UsersListViewModel by viewModel()
    private var usersActivityContext: FragmentActivity? = null
    private lateinit var viewBinding: FragmentUsersListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        usersActivityContext = requireActivity()
        if (savedInstanceState == null) {
            fetchUsers()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentUsersListBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModelDataChanges()
        setupListeners()
    }

    private fun fetchUsers() {
        usersActivityContext?.apply {
            viewModel.fetchUsers(NetworkHelper.hasConnection(context = this))
        }
    }

    private fun setupListeners() {
        viewBinding.apply {
            errorContent.retryButton.setOnClickListener {
                fetchUsers()
            }
        }
    }

    private fun setupUsersListRecyclerView(users: List<UserUiModel>) {
        viewBinding.usersListContent.usersList.adapter = UsersListAdapter(users)
    }

    private fun observeViewModelDataChanges() {
        usersActivityContext?.apply {
            viewModel.viewStateLiveData.observe(requireActivity(), Observer { viewState ->
                when (viewState) {
                    is ViewState.Success -> {
                        showUsersListPopulatedContent(viewState.data)
                    }
                    is ViewState.Loading -> {
                        showLoading()
                    }
                    is ViewState.Empty -> {
                        showUsersListEmptyContent()
                    }
                    is ViewState.InvalidParam -> {
                        // For showing params validations messages
                    }
                    is ViewState.Error -> {
                        showErrorContent(viewState.errorMessageResource)
                    }
                }
            })
        }
    }

    private fun showLoading() {
        viewBinding.apply {
            usersListContent.usersListLayout.gone()
            errorContent.errorLayout.gone()
            loadingContent.loadingLayout.visible()
        }
    }

    private fun showUsersListPopulatedContent(users: List<UserUiModel>) {
        setupUsersListRecyclerView(users)
        viewBinding.apply {
            errorContent.errorLayout.gone()
            loadingContent.loadingLayout.gone()
            usersListContent.noUsersText.gone()
            usersListContent.usersList.visible()
            usersListContent.usersListLayout.visible()
        }
    }

    private fun showUsersListEmptyContent() {
        viewBinding.apply {
            errorContent.errorLayout.gone()
            loadingContent.loadingLayout.gone()
            usersListContent.usersList.gone()
            usersListContent.noUsersText.visible()
            usersListContent.usersListLayout.visible()
        }
    }

    private fun showErrorContent(messageErrorResource: Int) {
        viewBinding.apply {
            usersListContent.usersListLayout.gone()
            loadingContent.loadingLayout.gone()
            errorContent.errorMessage.text = getString(messageErrorResource)
            errorContent.errorLayout.visible()
        }
    }
}