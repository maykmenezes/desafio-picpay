package com.maykmenezes.common.presentation

sealed class ViewState<T> {
    class Success<T>(val data: T) : ViewState<T>()
    class Loading<T> : ViewState<T>()
    class Empty<T> : ViewState<T>()
    class InvalidParam<T>(val messagesResource: List<Int?>) : ViewState<T>()
    class Error<T>(val errorMessageResource: Int) : ViewState<T>()
}