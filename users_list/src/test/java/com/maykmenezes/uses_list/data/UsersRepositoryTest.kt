package com.maykmenezes.uses_list.data

import com.maykmenezes.common.genericResult.GenericErrorResult
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.common.genericResult.GenericSuccessResult
import com.maykmenezes.users_list.data.dataSource.local.UsersLocalDataSource
import com.maykmenezes.users_list.data.dataSource.remote.UsersRemoteDataSource
import com.maykmenezes.users_list.data.mapper.toUserDomainModel
import com.maykmenezes.users_list.data.mapper.toUsersDomainModel
import com.maykmenezes.users_list.data.repository.UsersRepositoryImpl
import com.maykmenezes.users_list.domain.model.UserDomainModel
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.coVerifyOrder
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

internal class UsersRepositoryTest {

    private val localDataSource = mockk<UsersLocalDataSource>(relaxed = true)
    private val remoteDataSource = mockk<UsersRemoteDataSource>(relaxed = true)
    private val repository = UsersRepositoryImpl(
        localDataSource = localDataSource,
        remoteDataSource = remoteDataSource
    )

    @Test
    fun `Given populated success result of remote data source, when fetch users then should call remote data source and return populated success result`() =
        runBlocking {
            // Given
            coEvery { localDataSource.fetchUsers() } returns UsersRepositoryTestHelper.populatedLocalSuccessResult
            coEvery { remoteDataSource.fetchUsers() } returns UsersRepositoryTestHelper.populatedRemoteSuccessResult
            // When
            val result = repository.fetchUsers(UsersRepositoryTestHelper.hasConnection)
            // Then
            coVerify(exactly = 0) { localDataSource.fetchUsers() }
            coVerify(exactly = 1) { remoteDataSource.fetchUsers() }
            Assert.assertTrue(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Param)
            if (result is GenericResult.Success) {
                result.apply {
                    Assert.assertTrue(success is GenericSuccessResult.Populated)
                    Assert.assertFalse(success is GenericSuccessResult.Empty)
                    if (success is GenericSuccessResult.Populated) {
                        Assert.assertEquals(
                            (success as GenericSuccessResult.Populated<List<UserDomainModel>>).data,
                            UsersRepositoryTestHelper.populatedRemoteSuccessResult.toUsersDomainModel()
                        )
                    }
                }
            }
        }

    @Test
    fun `Given empty success result of remote data source, when fetch users then should call remote data source and return populated success result`() =
        runBlocking {
            // Given
            coEvery { localDataSource.fetchUsers() } returns UsersRepositoryTestHelper.populatedLocalSuccessResult
            coEvery { remoteDataSource.fetchUsers() } returns UsersRepositoryTestHelper.emptyRemoteSuccessResult
            // When
            val result = repository.fetchUsers(UsersRepositoryTestHelper.hasConnection)
            // Then
            coVerify(exactly = 0) { localDataSource.fetchUsers() }
            coVerify(exactly = 1) { remoteDataSource.fetchUsers() }
            Assert.assertTrue(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Param)
            if (result is GenericResult.Success) {
                Assert.assertTrue(result.success is GenericSuccessResult.Empty)
                Assert.assertFalse(result.success is GenericSuccessResult.Populated)
            }
        }

    @Test
    fun `Given exception of remote data source and has cache, when fetch users then should return populated success result`() =
        runBlocking {
            // Given
            coEvery { localDataSource.fetchUsers() } returns UsersRepositoryTestHelper.populatedLocalSuccessResult
            coEvery { remoteDataSource.fetchUsers() } throws Exception()
            // When
            val result = repository.fetchUsers(UsersRepositoryTestHelper.hasConnection)
            // Then
            coVerifyOrder {
                remoteDataSource.fetchUsers()
                localDataSource.fetchUsers()
            }
            Assert.assertTrue(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Param)
            if (result is GenericResult.Success) {
                result.apply {
                    Assert.assertTrue(success is GenericSuccessResult.Populated)
                    Assert.assertFalse(success is GenericSuccessResult.Empty)
                    if (success is GenericSuccessResult.Populated) {
                        Assert.assertEquals(
                            (success as GenericSuccessResult.Populated<List<UserDomainModel>>).data,
                            UsersRepositoryTestHelper.populatedLocalSuccessResult.toUserDomainModel()
                        )
                    }
                }
            }
        }

    @Test
    fun `Given exception of remote data source and not has cache, when fetch users then should return not found error result`() =
        runBlocking {
            // Given
            coEvery { localDataSource.fetchUsers() } returns UsersRepositoryTestHelper.emptyLocalSuccessResult
            coEvery { remoteDataSource.fetchUsers() } throws UsersRepositoryTestHelper.notFoundError()
            // When
            val result = repository.fetchUsers(UsersRepositoryTestHelper.hasConnection)
            // Then
            coVerifyOrder {
                remoteDataSource.fetchUsers()
                localDataSource.fetchUsers()
            }
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.Unknown)
                Assert.assertFalse(result.error is GenericErrorResult.Timeout)
                Assert.assertFalse(result.error is GenericErrorResult.NoConnection)
            }
        }

    @Test
    fun `Given exception of remote data source and not has cache, when fetch users then should return unauthorized error result`() =
        runBlocking {
            // Given
            coEvery { localDataSource.fetchUsers() } returns UsersRepositoryTestHelper.emptyLocalSuccessResult
            coEvery { remoteDataSource.fetchUsers() } throws UsersRepositoryTestHelper.unauthorizedFoundError()
            // When
            val result = repository.fetchUsers(UsersRepositoryTestHelper.hasConnection)
            // Then
            coVerifyOrder {
                remoteDataSource.fetchUsers()
                localDataSource.fetchUsers()
            }
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Unknown)
                Assert.assertFalse(result.error is GenericErrorResult.Timeout)
                Assert.assertFalse(result.error is GenericErrorResult.NoConnection)

            }
        }

    @Test
    fun `Given exception of remote data source and not has cache, when fetch users then should return unknown error result`() =
        runBlocking {
            // Given
            coEvery { localDataSource.fetchUsers() } returns UsersRepositoryTestHelper.emptyLocalSuccessResult
            coEvery { remoteDataSource.fetchUsers() } throws UsersRepositoryTestHelper.unknownError()
            // When
            val result = repository.fetchUsers(UsersRepositoryTestHelper.hasConnection)
            // Then
            coVerifyOrder {
                remoteDataSource.fetchUsers()
                localDataSource.fetchUsers()
            }
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.Unknown)
                Assert.assertFalse(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Timeout)
                Assert.assertFalse(result.error is GenericErrorResult.NoConnection)
            }
        }

    @Test
    fun `When fetch users no internet connection then should return no connection error result`() =
        runBlocking {
            // Given
            coEvery { localDataSource.fetchUsers() } returns UsersRepositoryTestHelper.emptyLocalSuccessResult
            coEvery { remoteDataSource.fetchUsers() } returns UsersRepositoryTestHelper.populatedRemoteSuccessResult
            // When
            val result = repository.fetchUsers(UsersRepositoryTestHelper.hasConnection.not())
            // Then
            coVerify(exactly = 0) { remoteDataSource.fetchUsers() }
            coVerify(exactly = 1) { localDataSource.fetchUsers() }
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.NoConnection)
                Assert.assertFalse(result.error is GenericErrorResult.Unknown)
                Assert.assertFalse(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Timeout)
            }
        }

    @Test
    fun `Given timeout error of remote data source and not has cache, when fetch users then should return timeout error result`() =
        runBlocking {
            // Given
            coEvery { localDataSource.fetchUsers() } returns UsersRepositoryTestHelper.emptyLocalSuccessResult
            coEvery { remoteDataSource.fetchUsers() } throws UsersRepositoryTestHelper.timeoutError()
            // When
            val result = repository.fetchUsers(UsersRepositoryTestHelper.hasConnection)
            // Then
            coVerifyOrder {
                remoteDataSource.fetchUsers()
                localDataSource.fetchUsers()
            }
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.Timeout)
                Assert.assertFalse(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Unknown)
                Assert.assertFalse(result.error is GenericErrorResult.NoConnection)
            }
        }
}