package com.maykmenezes.users_list.domain.useCase

import com.maykmenezes.common.domain.UseCase
import com.maykmenezes.users_list.data.repository.UsersRepository

internal class DeleteAllUsersUseCase(
    private val repository: UsersRepository
) : UseCase<Unit, Unit> {

    override suspend fun execute(params: Unit?) = repository.deleteAllUsers()
}