package com.maykmenezes.users_list.domain.model

internal data class FetchUsersRequest(
    val hasConnection: Boolean
)