package com.maykmenezes.users_list.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maykmenezes.common.presentation.ViewState
import com.maykmenezes.users_list.domain.model.UserDomainModel
import com.maykmenezes.users_list.domain.useCase.FetchUsersUseCase
import com.maykmenezes.users_list.presentation.model.UserUiModel
import com.maykmenezes.common.genericResult.GenericErrorResult
import com.maykmenezes.common.genericResult.GenericParamResult
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.common.genericResult.GenericSuccessResult
import com.maykmenezes.users_list.R
import com.maykmenezes.users_list.domain.model.InsertUserRequest
import com.maykmenezes.users_list.domain.useCase.DeleteAllUsersUseCase
import com.maykmenezes.users_list.domain.useCase.InsertUserUseCase
import com.maykmenezes.users_list.presentation.mapper.toFetchUsersRequest
import com.maykmenezes.users_list.presentation.mapper.toInsertUserRequest
import com.maykmenezes.users_list.presentation.mapper.toUsersUiModel
import kotlinx.coroutines.launch

internal class UsersListViewModel(
    private val fetchUsersUseCase: FetchUsersUseCase,
    private val insertUserUseCase: InsertUserUseCase,
    private val deleteAllUsersUseCase: DeleteAllUsersUseCase
) : ViewModel() {

    private val _viewStateLiveData = MutableLiveData<ViewState<List<UserUiModel>>>()
    val viewStateLiveData: LiveData<ViewState<List<UserUiModel>>> = _viewStateLiveData

    fun fetchUsers(hasConnection: Boolean) {
        _viewStateLiveData.value = ViewState.Loading()
        viewModelScope.launch {
            when (val result =
                fetchUsersUseCase.execute(params = hasConnection.toFetchUsersRequest())) {
                is GenericResult.Success -> {
                    when (result.success) {
                        is GenericSuccessResult.Populated -> {
                            val usersDomainModel =
                                (result.success as GenericSuccessResult.Populated<List<UserDomainModel>>).data
                            usersDomainModel.toUsersUiModel().let { users ->
                                _viewStateLiveData.value = ViewState.Success(users)
                                updateLocalCache(users)
                            }
                        }
                        is GenericSuccessResult.Empty -> {
                            _viewStateLiveData.value = ViewState.Empty()
                        }
                    }
                }
                is GenericResult.Param -> {
                    when (result.param) {
                        is GenericParamResult.Invalid -> {
                            val messages =
                                (result.param as GenericParamResult.Invalid<List<UserDomainModel>>).messages
                            _viewStateLiveData.value = ViewState.InvalidParam(messages)
                        }
                    }
                }
                is GenericResult.Error -> {
                    when (result.error) {
                        is GenericErrorResult.NoConnection -> {
                            _viewStateLiveData.value =
                                ViewState.Error(errorMessageResource = R.string.no_connection_error_message)
                        }
                        is GenericErrorResult.NotFound -> {
                            _viewStateLiveData.value =
                                ViewState.Error(errorMessageResource = R.string.not_found_error_message)
                        }
                        is GenericErrorResult.Unauthorized -> {
                            _viewStateLiveData.value =
                                ViewState.Error(errorMessageResource = R.string.unauthorized_error_message)
                        }
                        is GenericErrorResult.Unknown -> {
                            _viewStateLiveData.value =
                                ViewState.Error(errorMessageResource = R.string.server_error_message)
                        }
                        is GenericErrorResult.Timeout -> {
                            _viewStateLiveData.value =
                                ViewState.Error(errorMessageResource = R.string.timeout_error_message)
                        }
                    }
                }
            }
        }
    }

    private fun updateLocalCache(users: List<UserUiModel>) {
        viewModelScope.launch {
            when (deleteAllUsersUseCase.execute()) {
                is GenericResult.Success -> {
                    repeat(users.count()) { userIndex ->
                        insertUser(users[userIndex].toInsertUserRequest())
                    }
                }
                else -> {
                    // do nothing
                }
            }
        }
    }

    private fun insertUser(userRequest: InsertUserRequest) {
        viewModelScope.launch {
            insertUserUseCase.execute(userRequest)
        }
    }
}