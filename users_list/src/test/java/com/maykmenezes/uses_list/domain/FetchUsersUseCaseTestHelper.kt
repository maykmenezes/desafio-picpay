package com.maykmenezes.uses_list.domain

import com.maykmenezes.common.genericResult.GenericErrorResult
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.common.genericResult.GenericSuccessResult
import com.maykmenezes.users_list.domain.model.FetchUsersRequest
import com.maykmenezes.users_list.domain.model.UserDomainModel

internal object FetchUsersUseCaseTestHelper {

    val populatedSuccessResult = GenericResult.Success(
        GenericSuccessResult.Populated(
            listOf(
                UserDomainModel(
                    id = 1,
                    name = "Robert",
                    urlImage = "https://randomuser.me/api/portraits/men/1.jpg",
                    userName = "@Robert"
                ),
                UserDomainModel(
                    id = 2,
                    name = "Leon",
                    urlImage = "https://randomuser.me/api/portraits/men/2.jpg",
                    userName = "@Leon"
                ),
                UserDomainModel(
                    id = 3,
                    name = "Lian",
                    urlImage = "https://randomuser.me/api/portraits/men/3.jpg",
                    userName = "@Lian"
                )
            )
        )
    )

    val emptySuccessResult =
        GenericResult.Success<List<UserDomainModel>>(GenericSuccessResult.Empty())

    val notFoundErrorResult =
        GenericResult.Error<List<UserDomainModel>>(GenericErrorResult.NotFound(Exception()))

    val unauthorizedFoundErrorResult =
        GenericResult.Error<List<UserDomainModel>>(GenericErrorResult.Unauthorized(Exception()))

    val unknownErrorResult =
        GenericResult.Error<List<UserDomainModel>>(GenericErrorResult.Unknown(Exception()))

    val timeoutErrorResult =
        GenericResult.Error<List<UserDomainModel>>(GenericErrorResult.Timeout(Exception()))

    val noConnectionErrorResult =
        GenericResult.Error<List<UserDomainModel>>(GenericErrorResult.NoConnection())

    val invalidParam = null
    val hasConnectionRequest = FetchUsersRequest(hasConnection = true)
    val noConnectionRequest = FetchUsersRequest(hasConnection = false)
}