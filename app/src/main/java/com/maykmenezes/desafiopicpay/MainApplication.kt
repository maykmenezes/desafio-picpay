package com.maykmenezes.desafiopicpay

import android.app.Application
import com.maykmenezes.desafiopicpay.AppDiModules.appDiModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

internal class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MainApplication)
            modules(appDiModules)
        }
    }
}