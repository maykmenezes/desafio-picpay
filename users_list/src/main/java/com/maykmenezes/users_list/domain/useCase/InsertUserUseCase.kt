package com.maykmenezes.users_list.domain.useCase

import com.maykmenezes.common.domain.UseCase
import com.maykmenezes.common.genericResult.GenericParamResult
import com.maykmenezes.users_list.data.repository.UsersRepository
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.users_list.R
import com.maykmenezes.users_list.domain.model.InsertUserRequest

internal class InsertUserUseCase(
    private val repository: UsersRepository
) : UseCase<InsertUserRequest, Unit> {

    override suspend fun execute(params: InsertUserRequest?): GenericResult<Unit> {
        params?.let { user ->
            validateParam(user)?.let { messages ->
                return GenericResult.Param(GenericParamResult.Invalid(messages = messages))
            }
            repository.insertUser(user)
        }
        return GenericResult.Param(GenericParamResult.Invalid(messages = listOf(R.string.generic_invalid_params_text)))
    }

    // Params validation if needed
    private fun validateParam(user: InsertUserRequest): List<Int>? {
        // business rule validation
        return if (user.name.isBlank()) {
            listOf(R.string.generic_invalid_params_text)
        } else {
            null
        }
    }
}