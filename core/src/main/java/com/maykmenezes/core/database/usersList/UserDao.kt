package com.maykmenezes.core.database.usersList

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(user: UserEntity)

    @Query("SELECT * FROM user_table")
    suspend fun fetchUsers(): List<UserEntity>

    @Query("DELETE FROM user_table")
    suspend fun deleteAllUsers()
}