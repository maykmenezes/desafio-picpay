package com.maykmenezes.uses_list.presentation

import com.maykmenezes.common.genericResult.GenericErrorResult
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.common.genericResult.GenericSuccessResult
import com.maykmenezes.users_list.domain.model.UserDomainModel

internal object UsersListViewModelTestHelper {

    val populatedSuccessDomainData = listOf(
        UserDomainModel(
            id = 1,
            name = "Robert",
            urlImage = "https://randomuser.me/api/portraits/men/1.jpg",
            userName = "@Robert"
        ),
        UserDomainModel(
            id = 2,
            name = "Leon",
            urlImage = "https://randomuser.me/api/portraits/men/2.jpg",
            userName = "@Leon"
        ),
        UserDomainModel(
            id = 3,
            name = "Lian",
            urlImage = "https://randomuser.me/api/portraits/men/3.jpg",
            userName = "@Lian"
        )
    )

    val useCasePopulatedSuccessResult = GenericResult.Success(
        GenericSuccessResult.Populated(populatedSuccessDomainData)
    )

    val useCaseEmptySuccessResult = GenericResult.Success(
        GenericSuccessResult.Empty<List<UserDomainModel>>()
    )

    val useCaseNotFoundErrorResult = GenericResult.Error(
        GenericErrorResult.NotFound<List<UserDomainModel>>(Exception())
    )

    val useCaseUnauthorizedErrorResult = GenericResult.Error(
        GenericErrorResult.Unauthorized<List<UserDomainModel>>(Exception())
    )

    val useCaseUnknownErrorResult = GenericResult.Error(
        GenericErrorResult.Unknown<List<UserDomainModel>>(Exception())
    )

    val useCaseTimeoutErrorResult = GenericResult.Error(
        GenericErrorResult.Timeout<List<UserDomainModel>>(Exception())
    )

    val useCaseNoConnectionErrorResult = GenericResult.Error(
        GenericErrorResult.NoConnection<List<UserDomainModel>>()
    )
}