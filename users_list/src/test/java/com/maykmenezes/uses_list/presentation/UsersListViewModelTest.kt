package com.maykmenezes.uses_list.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.maykmenezes.common.presentation.ViewState
import com.maykmenezes.users_list.R
import com.maykmenezes.users_list.domain.useCase.DeleteAllUsersUseCase
import com.maykmenezes.users_list.domain.useCase.FetchUsersUseCase
import com.maykmenezes.users_list.domain.useCase.InsertUserUseCase
import com.maykmenezes.users_list.presentation.UsersListViewModel
import com.maykmenezes.users_list.presentation.mapper.toUsersUiModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
internal class UsersListViewModelTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: UsersListViewModel

    private val fetchUsersUseCase = mockk<FetchUsersUseCase>(relaxed = true)
    private val insertUserUseCase = mockk<InsertUserUseCase>(relaxed = true)
    private val deleteAllUsersUseCase = mockk<DeleteAllUsersUseCase>(relaxed = true)

    @Before
    fun setup() {
        viewModel = UsersListViewModel(fetchUsersUseCase, insertUserUseCase, deleteAllUsersUseCase)
    }

    @Test
    fun `Given populated success use case result, when fetch users then should return success state and same data result`() =
        mainCoroutineRule.runBlockingTest {
            // Given
            coEvery { fetchUsersUseCase.execute(any()) } returns UsersListViewModelTestHelper.useCasePopulatedSuccessResult
            // When
            viewModel.fetchUsers(true)
            // Then
            val finalState = viewModel.viewStateLiveData.value
            Assert.assertTrue(finalState is ViewState.Success)
            Assert.assertFalse(finalState is ViewState.Loading)
            Assert.assertFalse(finalState is ViewState.Empty)
            Assert.assertFalse(finalState is ViewState.InvalidParam)
            Assert.assertFalse(finalState is ViewState.Error)
            if (finalState is ViewState.Success) {
                finalState.apply {
                    Assert.assertEquals(
                        UsersListViewModelTestHelper.populatedSuccessDomainData.toUsersUiModel(),
                        data
                    )
                }
            }
        }

    @Test
    fun `Given empty success use case result, when fetch users then should return empty state`() =
        mainCoroutineRule.runBlockingTest {
            // Given
            coEvery { fetchUsersUseCase.execute(any()) } returns UsersListViewModelTestHelper.useCaseEmptySuccessResult
            // When
            viewModel.fetchUsers(true)
            // Then
            val finalState = viewModel.viewStateLiveData.value
            Assert.assertTrue(finalState is ViewState.Empty)
            Assert.assertFalse(finalState is ViewState.Loading)
            Assert.assertFalse(finalState is ViewState.Success)
            Assert.assertFalse(finalState is ViewState.InvalidParam)
            Assert.assertFalse(finalState is ViewState.Error)
        }

    @Test
    fun `Given not found use case error result, when fetch users then should return error state and not found message error`() =
        mainCoroutineRule.runBlockingTest {
            // Given
            coEvery { fetchUsersUseCase.execute(any()) } returns UsersListViewModelTestHelper.useCaseNotFoundErrorResult
            // When
            viewModel.fetchUsers(true)
            // Then
            val finalState = viewModel.viewStateLiveData.value
            Assert.assertTrue(finalState is ViewState.Error)
            Assert.assertFalse(finalState is ViewState.Loading)
            Assert.assertFalse(finalState is ViewState.Success)
            Assert.assertFalse(finalState is ViewState.InvalidParam)
            Assert.assertFalse(finalState is ViewState.Empty)
            if (finalState is ViewState.Error) {
                finalState.apply {
                    Assert.assertEquals(errorMessageResource, R.string.not_found_error_message)
                }
            }
        }

    @Test
    fun `Given unauthorized error use case result, when fetch users then should return error state and unauthorized message error`() =
        mainCoroutineRule.runBlockingTest {
            // Given
            coEvery { fetchUsersUseCase.execute(any()) } returns UsersListViewModelTestHelper.useCaseUnauthorizedErrorResult
            // When
            viewModel.fetchUsers(true)
            // Then
            val finalState = viewModel.viewStateLiveData.value
            Assert.assertTrue(finalState is ViewState.Error)
            Assert.assertFalse(finalState is ViewState.Loading)
            Assert.assertFalse(finalState is ViewState.Success)
            Assert.assertFalse(finalState is ViewState.InvalidParam)
            Assert.assertFalse(finalState is ViewState.Empty)
            if (finalState is ViewState.Error) {
                finalState.apply {
                    Assert.assertEquals(errorMessageResource, R.string.unauthorized_error_message)
                }
            }
        }

    @Test
    fun `Given unknown error use case result, when fetch users then should return error state and unknown message error`() =
        mainCoroutineRule.runBlockingTest {
            // Given
            coEvery { fetchUsersUseCase.execute(any()) } returns UsersListViewModelTestHelper.useCaseUnknownErrorResult
            // When
            viewModel.fetchUsers(true)
            // Then
            val finalState = viewModel.viewStateLiveData.value
            Assert.assertTrue(finalState is ViewState.Error)
            Assert.assertFalse(finalState is ViewState.Loading)
            Assert.assertFalse(finalState is ViewState.Success)
            Assert.assertFalse(finalState is ViewState.InvalidParam)
            Assert.assertFalse(finalState is ViewState.Empty)
            if (finalState is ViewState.Error) {
                finalState.apply {
                    Assert.assertEquals(errorMessageResource, R.string.server_error_message)
                }
            }
        }

    @Test
    fun `Given timeout error use case result, when fetch users then should return error state and timeout message error`() =
        mainCoroutineRule.runBlockingTest {
            // Given
            coEvery { fetchUsersUseCase.execute(any()) } returns UsersListViewModelTestHelper.useCaseTimeoutErrorResult
            // When
            viewModel.fetchUsers(true)
            // Then
            val finalState = viewModel.viewStateLiveData.value
            Assert.assertTrue(finalState is ViewState.Error)
            Assert.assertFalse(finalState is ViewState.Loading)
            Assert.assertFalse(finalState is ViewState.Success)
            Assert.assertFalse(finalState is ViewState.InvalidParam)
            Assert.assertFalse(finalState is ViewState.Empty)
            if (finalState is ViewState.Error) {
                finalState.apply {
                    Assert.assertEquals(errorMessageResource, R.string.timeout_error_message)
                }
            }
        }

    @Test
    fun `Given no connection error use case result, when fetch users then should return error state and no connection message error`() =
        mainCoroutineRule.runBlockingTest {
            // Given
            coEvery { fetchUsersUseCase.execute(any()) } returns UsersListViewModelTestHelper.useCaseNoConnectionErrorResult
            // When
            viewModel.fetchUsers(true)
            // Then
            val finalState = viewModel.viewStateLiveData.value
            Assert.assertTrue(finalState is ViewState.Error)
            Assert.assertFalse(finalState is ViewState.Loading)
            Assert.assertFalse(finalState is ViewState.Success)
            Assert.assertFalse(finalState is ViewState.InvalidParam)
            Assert.assertFalse(finalState is ViewState.Empty)
            if (finalState is ViewState.Error) {
                finalState.apply {
                    Assert.assertEquals(errorMessageResource, R.string.no_connection_error_message)
                }
            }
        }
}