package com.maykmenezes.users_list.domain.useCase

import com.maykmenezes.common.domain.UseCase
import com.maykmenezes.common.genericResult.GenericParamResult
import com.maykmenezes.users_list.data.repository.UsersRepository
import com.maykmenezes.users_list.domain.model.UserDomainModel
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.users_list.R
import com.maykmenezes.users_list.domain.model.FetchUsersRequest

internal class FetchUsersUseCase(
    private val repository: UsersRepository
) : UseCase<FetchUsersRequest, List<UserDomainModel>> {

    override suspend fun execute(params: FetchUsersRequest?): GenericResult<List<UserDomainModel>> {
        params?.let { param ->
            return repository.fetchUsers(param.hasConnection)
        }
        return GenericResult.Param(GenericParamResult.Invalid(messages = listOf(R.string.generic_invalid_params_text)))
    }
}