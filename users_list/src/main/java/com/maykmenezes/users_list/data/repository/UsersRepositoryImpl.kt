package com.maykmenezes.users_list.data.repository

import com.maykmenezes.common.genericResult.GenericErrorResult
import com.maykmenezes.users_list.data.dataSource.local.UsersLocalDataSource
import com.maykmenezes.users_list.data.dataSource.remote.UsersRemoteDataSource
import com.maykmenezes.users_list.domain.model.UserDomainModel
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.common.genericResult.GenericSuccessResult
import com.maykmenezes.common.genericResult.GenericErrorResultMapper
import com.maykmenezes.users_list.data.mapper.toUserDomainModel
import com.maykmenezes.users_list.data.mapper.toUserEntity
import com.maykmenezes.users_list.data.mapper.toUsersDomainModel
import com.maykmenezes.users_list.domain.model.InsertUserRequest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class UsersRepositoryImpl(
    private val localDataSource: UsersLocalDataSource,
    private val remoteDataSource: UsersRemoteDataSource
) : UsersRepository {

    override suspend fun fetchUsers(hasConnection: Boolean): GenericResult<List<UserDomainModel>> {
        return withContext(Dispatchers.IO) {
            if (hasConnection.not()) {
                fetchUsersFromLocalDatabase(
                    GenericResult.Error(GenericErrorResult.NoConnection())
                )
            } else {
                try {
                    remoteDataSource.fetchUsers().let { usersList ->
                        if (usersList.isEmpty()) {
                            GenericResult.Success(success = GenericSuccessResult.Empty())
                        } else {
                            GenericResult.Success(
                                success = GenericSuccessResult.Populated(usersList.toUsersDomainModel())
                            )
                        }
                    }
                } catch (exception: Exception) {
                    fetchUsersFromLocalDatabase(
                        GenericErrorResultMapper.exceptionErrorToGenericErrorResult(exception)
                    )
                }
            }
        }
    }

    override suspend fun insertUser(request: InsertUserRequest) {
        withContext(Dispatchers.IO) {
            try {
                localDataSource.insertUser(request.toUserEntity())
            } catch (exception: Exception) {
                // do nothing
            }
        }
    }

    override suspend fun deleteAllUsers(): GenericResult<Unit> {
        return withContext(Dispatchers.IO) {
            try {
                GenericResult.Success(GenericSuccessResult.Populated(localDataSource.deleteAllUsers()))
            } catch (exception: Exception) {
                GenericResult.Error(GenericErrorResult.Unknown(exception))
            }
        }
    }

    private suspend fun fetchUsersFromLocalDatabase(
        genericErrorResult: GenericResult<List<UserDomainModel>>
    ): GenericResult<List<UserDomainModel>> {
        return withContext(Dispatchers.IO) {
            try {
                localDataSource.fetchUsers().let { users ->
                    if (users.isEmpty()) {
                        genericErrorResult
                    } else {
                        GenericResult.Success(
                            success = GenericSuccessResult.Populated(users.toUserDomainModel())
                        )
                    }
                }
            } catch (exception: Exception) {
                genericErrorResult
            }
        }
    }
}