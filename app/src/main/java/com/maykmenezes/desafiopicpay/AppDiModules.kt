package com.maykmenezes.desafiopicpay

import com.maykmenezes.users_list.domain.di.UsersListDiModule
import com.maykmenezes.core.di.CoreDiModule

internal object AppDiModules {

    val appDiModules = listOf(
        // Core module
        CoreDiModule.instance,
        // Features modules
        UsersListDiModule.instance
    )
}