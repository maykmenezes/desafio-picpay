package com.maykmenezes.users_list.domain.model

internal data class InsertUserRequest(
    val id: Int,
    val name: String,
    val urlImage: String,
    val userName: String
)