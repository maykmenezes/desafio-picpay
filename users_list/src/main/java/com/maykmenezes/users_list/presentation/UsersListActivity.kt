package com.maykmenezes.users_list.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.maykmenezes.users_list.R

internal class UsersListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users_list)
    }
}