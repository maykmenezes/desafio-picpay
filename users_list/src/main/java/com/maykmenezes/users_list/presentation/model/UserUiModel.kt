package com.maykmenezes.users_list.presentation.model

internal data class UserUiModel(
    val id: Int,
    val name: String,
    val urlImage: String,
    val userName: String
)