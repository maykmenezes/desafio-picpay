package com.maykmenezes.users_list.presentation

import androidx.recyclerview.widget.RecyclerView
import com.maykmenezes.helper.extensionFunction.downloadImage
import com.maykmenezes.users_list.R
import com.maykmenezes.users_list.databinding.UserItemLayoutBinding
import com.maykmenezes.users_list.presentation.model.UserUiModel

internal class UsersListAdapterViewHolder(private val viewBinding: UserItemLayoutBinding) :
    RecyclerView.ViewHolder(viewBinding.root) {

    fun bindView(user: UserUiModel) {
        viewBinding.apply {
            image.downloadImage(
                urlImage = user.urlImage,
                placeholderImageResource = R.drawable.user_placeholder_image_icon
            )
            userName.text = setupUserName(user.userName)
            name.text = user.name
        }
    }

    private fun setupUserName(name: String) = "@$name"
}