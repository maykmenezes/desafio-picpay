package com.maykmenezes.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.maykmenezes.core.database.usersList.UserDao
import com.maykmenezes.core.database.usersList.UserEntity

@Database(entities = [UserEntity::class], version = 1)
internal abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}