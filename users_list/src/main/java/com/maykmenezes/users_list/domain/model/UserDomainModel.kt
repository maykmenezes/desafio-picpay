package com.maykmenezes.users_list.domain.model

internal data class UserDomainModel(
    val id: Int,
    val name: String,
    val urlImage: String,
    val userName: String
)