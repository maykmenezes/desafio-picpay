package com.maykmenezes.sample

import android.app.Application
import com.maykmenezes.users_list.domain.di.UsersListDiModule
import com.maykmenezes.core.di.CoreDiModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

internal class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MainApplication)
            modules(listOf(CoreDiModule.instance, UsersListDiModule.instance))
        }
    }
}