package com.maykmenezes.users_list

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.maykmenezes.users_list.data.dataSource.local.UsersLocalDataSource
import com.maykmenezes.users_list.data.dataSource.remote.UsersApi
import com.maykmenezes.users_list.data.dataSource.remote.UsersRemoteDataSource
import com.maykmenezes.users_list.data.model.UserResponse
import com.maykmenezes.users_list.data.repository.UsersRepository
import com.maykmenezes.users_list.data.repository.UsersRepositoryImpl
import com.maykmenezes.users_list.domain.useCase.FetchUsersUseCase
import com.maykmenezes.users_list.domain.useCase.InsertUserUseCase
import com.maykmenezes.users_list.presentation.UsersListViewModel
import com.maykmenezes.core.database.usersList.UserEntity
import com.maykmenezes.core.di.CoreDiModule
import com.maykmenezes.users_list.domain.useCase.DeleteAllUsersUseCase
import com.maykmenezes.users_list.presentation.UsersListFragment
import io.mockk.coEvery
import io.mockk.mockk
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.KoinContextHandler
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit

internal object UsersListFragmentTestRobot {

    private lateinit var scenario: FragmentScenario<UsersListFragment>
    private val localDataSource = mockk<UsersLocalDataSource>(relaxed = true)
    private val remoteDataSource = mockk<UsersRemoteDataSource>(relaxed = true)

    fun launchScreen() {
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_DesafioPicPay)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    fun mockPopulatedSuccessResult(): List<UserResponse> {
        val remoteDataSourceResponse =
            UsersListFragmentTestHelper.mockedRemoteDataSourcePopulatedSuccessResponse
        coEvery { localDataSource.fetchUsers() } returns UsersListFragmentTestHelper.mockedLocalDataSourceWithEmptySuccessResponse
        coEvery { remoteDataSource.fetchUsers() } returns remoteDataSourceResponse
        initKoin(setupMockedModule())
        return remoteDataSourceResponse
    }

    fun mockEmptySuccessResult() {
        coEvery { localDataSource.fetchUsers() } returns UsersListFragmentTestHelper.mockedLocalDataSourceWithEmptySuccessResponse
        coEvery { remoteDataSource.fetchUsers() } returns UsersListFragmentTestHelper.mockedRemoteDataSourceWithEmptySuccessResponse
        initKoin(setupMockedModule())
    }

    fun mockErrorResultWithCache(): List<UserEntity> {
        val localDataSourceResponse =
            UsersListFragmentTestHelper.mockedLocalDataSourcePopulatedSuccessResponse
        coEvery { localDataSource.fetchUsers() } returns localDataSourceResponse
        coEvery { remoteDataSource.fetchUsers() } throws Exception()
        initKoin(setupMockedModule())
        return localDataSourceResponse
    }

    fun mockNotFoundErrorResultNoCache() {
        val response = getErrorResponseByCode(404)
        coEvery { localDataSource.fetchUsers() } returns UsersListFragmentTestHelper.mockedLocalDataSourceWithEmptySuccessResponse
        coEvery { remoteDataSource.fetchUsers() } throws HttpException(response)
        initKoin(setupMockedModule())
    }

    fun mockUnauthorizedErrorResultNoCache() {
        val response = getErrorResponseByCode(401)
        coEvery { localDataSource.fetchUsers() } returns UsersListFragmentTestHelper.mockedLocalDataSourceWithEmptySuccessResponse
        coEvery { remoteDataSource.fetchUsers() } throws HttpException(response)
        initKoin(setupMockedModule())
    }

    fun mockUnknownErrorResultNoCache() {
        val response = getErrorResponseByCode(500)
        coEvery { localDataSource.fetchUsers() } returns UsersListFragmentTestHelper.mockedLocalDataSourceWithEmptySuccessResponse
        coEvery { remoteDataSource.fetchUsers() } throws HttpException(response)
        initKoin(setupMockedModule())
    }

    fun mockFullSuccessResult(): List<UserEntity> {
        val localDataSourceResponse =
            UsersListFragmentTestHelper.mockedLocalDataSourcePopulatedSuccessResponse
        coEvery { localDataSource.fetchUsers() } returns localDataSourceResponse
        coEvery { remoteDataSource.fetchUsers() } returns UsersListFragmentTestHelper.mockedRemoteDataSourcePopulatedSuccessResponse
        initKoin(setupMockedModule())
        return localDataSourceResponse
    }

    private fun getErrorResponseByCode(code: Int): Response<List<UserResponse>> {
        return Response.error(
            code, ResponseBody.create("application/json".toMediaTypeOrNull(), "")
        )
    }

    fun checkPopulatedRecyclerViewItemCount(childCount: Int) {
        onView(withId(R.id.usersList))
            .check(matches(ViewMatchers.hasChildCount(childCount)))
    }

    fun tapView(viewResource: Int) {
        onView(withId(viewResource)).perform(click())
    }

    fun checkEmptyLayout() {
        onView(withId(R.id.usersList)).check(
            matches(
                ViewMatchers.withEffectiveVisibility(
                    ViewMatchers.Visibility.GONE
                )
            )
        )
        onView(withId(R.id.noUsersText)).check(
            matches(
                ViewMatchers.withEffectiveVisibility(
                    ViewMatchers.Visibility.VISIBLE
                )
            )
        )
    }

    fun checkErrorLayout(errorMessageResource: Int) {
        onView(withId(R.id.loadingContent)).check(
            matches(
                ViewMatchers.withEffectiveVisibility(
                    ViewMatchers.Visibility.GONE
                )
            )
        )
        onView(withId(R.id.usersListContent)).check(
            matches(
                ViewMatchers.withEffectiveVisibility(
                    ViewMatchers.Visibility.GONE
                )
            )
        )
        onView(withId(R.id.errorContent)).check(
            matches(
                ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
            )
        )
        onView(withId(R.id.errorMessage)).check(
            matches(withText(getStringResource(errorMessageResource)))
        )
    }

    private fun getStringResource(id: Int) =
        InstrumentationRegistry.getInstrumentation().targetContext.resources.getString(id)

    private fun setupMockedModule(): Module {
        return module {
            single<UsersApi> { get<Retrofit>().create(UsersApi::class.java) }
            factory { localDataSource }
            factory { remoteDataSource }
            factory<UsersRepository> { UsersRepositoryImpl(get(), get()) }
            factory { FetchUsersUseCase(get()) }
            factory { InsertUserUseCase(get()) }
            factory { DeleteAllUsersUseCase(get()) }
            viewModel { UsersListViewModel(get(), get(), get()) }
        }
    }

    private fun initKoin(mockedModule: Module) {
        if (KoinContextHandler.getOrNull() == null) {
            startKoin {
                androidContext(InstrumentationRegistry.getInstrumentation().targetContext)
                modules(CoreDiModule.instance, mockedModule)
            }
        }
    }
}