package com.maykmenezes.common.domain

import com.maykmenezes.common.genericResult.GenericResult

interface UseCase<Param, Output> {
    suspend fun execute(params: Param? = null): GenericResult<Output>
}