package com.maykmenezes.users_list

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
internal class UsersListFragmentTest {

    private val robot = UsersListFragmentTestRobot
    private val hasConnection = UsersListFragmentTestHelper.hasConnection()

    @Test
    fun verifyRecyclerViewItemCount() {
        // Given
        val response = robot.mockPopulatedSuccessResult()
        // When
        robot.launchScreen()
        // Then
        if (hasConnection) {
            robot.checkPopulatedRecyclerViewItemCount(response.count())
        } else {
            robot.checkErrorLayout(R.string.no_connection_error_message)
        }
    }

    @Test
    fun verifyEmptyLayout() {
        // Given
        robot.mockEmptySuccessResult()
        // When
        robot.launchScreen()
        // Then
        if (hasConnection) {
            robot.checkEmptyLayout()
        } else {
            robot.checkErrorLayout(R.string.no_connection_error_message)
        }
    }

    @Test
    fun verifyRecyclerViewItemCountAfterCallCache() {
        // Given
        val response = robot.mockErrorResultWithCache()
        // When
        robot.launchScreen()
        // Then
        robot.checkPopulatedRecyclerViewItemCount(response.count())
    }

    @Test
    fun verifyNotFoundErrorLayout() {
        // Given
        robot.mockNotFoundErrorResultNoCache()
        // When
        robot.launchScreen()
        // Then
        if (hasConnection) {
            robot.checkErrorLayout(R.string.not_found_error_message)
        } else {
            robot.checkErrorLayout(R.string.no_connection_error_message)
        }
    }

    @Test
    fun verifyUnauthorizedErrorLayout() {
        // Given
        robot.mockUnauthorizedErrorResultNoCache()
        // When
        robot.launchScreen()
        // Then
        if (hasConnection) {
            robot.checkErrorLayout(R.string.unauthorized_error_message)
        } else {
            robot.checkErrorLayout(R.string.no_connection_error_message)
        }
    }

    @Test
    fun verifyUnknownErrorLayout() {
        // Given
        robot.mockUnknownErrorResultNoCache()
        // When
        robot.launchScreen()
        // Then
        if (hasConnection) {
            robot.checkErrorLayout(R.string.server_error_message)
        } else {
            robot.checkErrorLayout(R.string.no_connection_error_message)
        }
    }

    @Test
    fun verifyTryAgainAndSuccess() {
        // Given
        robot.mockUnknownErrorResultNoCache()
        // When
        robot.launchScreen()
        // Then
        if (hasConnection) {
            val response = robot.mockPopulatedSuccessResult()
            robot.tapView(R.id.retryButton)
            if (hasConnection) {
                robot.checkPopulatedRecyclerViewItemCount(response.count())
            } else {
                robot.checkErrorLayout(R.string.no_connection_error_message)
            }
        } else {
            robot.checkErrorLayout(R.string.no_connection_error_message)
        }
    }

    @Test
    fun verifyTryAgainAndError() {
        // Given
        robot.mockUnknownErrorResultNoCache()
        // When
        robot.launchScreen()
        // Then
        if (hasConnection) {
            robot.checkErrorLayout(R.string.server_error_message)
            robot.tapView(R.id.retryButton)
            if (hasConnection) {
                robot.checkErrorLayout(R.string.server_error_message)
            } else {
                robot.checkErrorLayout(R.string.no_connection_error_message)
            }
        } else {
            robot.checkErrorLayout(R.string.no_connection_error_message)
        }
    }

    @Test
    fun verifyNoConnectionErrorWithCache() {
        // Given
        val response = robot.mockFullSuccessResult()
        // When
        robot.launchScreen()
        // Then
        if (hasConnection) {
            robot.checkPopulatedRecyclerViewItemCount(response.count())
        } else {
            robot.checkPopulatedRecyclerViewItemCount(response.count())
        }
    }
}