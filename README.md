**Informações gerais sobre a solução para este desafio**

Projeto **modularizado** utilizando a abordagem de **module by feature**, onde cada feature possui o seu próprio modulo. Além dos módulos das features ainda existem os seguintes módulos: 

- **Core**: onde estão os arquivos de configuração do projeto como configuração da api, database e cliiente http. 
- **Common**: onde estão os arquivos comuns para todo o projeto, como resultados genéricos, constantes e outros recursos. 
- **Helper**: onde estão as funções que auxiliam em tarefas comuns no projeto, como as extensions functions. 
 

No meu ponto de vista a abordagem module by feature é indicada para grandes projetos com vários times, onde cada time é responsável por uma feature no projeto, onde uma feature pode ser desenvolvida sem dependência direta com outros módulos. 

Dentro do módulo da feature foi adotada a arquitetura **MVVM + Clean Architecture**, dividindo em 3 packages(camadas): **data, domain e presentation**, onde cada camada tem sua responsabilidade bem definida seguindo o Clean Architecture. Foi implementado testes em todas as camadas. 

Principais tecnologias utilizadas: 

- Koin: 2.1.6
- Retrofit2: 2.9.0
- Room database: 2.3.0
- Glide: 4.12.0
- JUnit: 4.13.1
- Mockk: 1.12.0
- Espresso: 3.3.0

