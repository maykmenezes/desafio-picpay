package com.maykmenezes.users_list.data.dataSource.remote

import com.maykmenezes.users_list.data.model.UserResponse

internal interface UsersRemoteDataSource {

    suspend fun fetchUsers(): List<UserResponse>
}