package com.maykmenezes.users_list.data.repository

import com.maykmenezes.users_list.domain.model.UserDomainModel
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.users_list.domain.model.InsertUserRequest

internal interface UsersRepository {

    suspend fun fetchUsers(hasConnection: Boolean): GenericResult<List<UserDomainModel>>
    suspend fun insertUser(request: InsertUserRequest)
    suspend fun deleteAllUsers(): GenericResult<Unit>
}