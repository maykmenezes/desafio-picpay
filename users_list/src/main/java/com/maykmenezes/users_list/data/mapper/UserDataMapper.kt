package com.maykmenezes.users_list.data.mapper

import com.maykmenezes.common.constants.Constants
import com.maykmenezes.core.database.usersList.UserEntity
import com.maykmenezes.users_list.data.model.UserResponse
import com.maykmenezes.users_list.domain.model.InsertUserRequest
import com.maykmenezes.users_list.domain.model.UserDomainModel

internal fun List<UserResponse>.toUsersDomainModel(): List<UserDomainModel> {
    val usersList = mutableListOf<UserDomainModel>()
    repeat(count()) { userIndex ->
        usersList.add(
            UserDomainModel(
                id = this[userIndex].id ?: 0,
                name = this[userIndex].name ?: Constants.EMPTY_STRING,
                urlImage = this[userIndex].urlImage ?: Constants.EMPTY_STRING,
                userName = this[userIndex].userName ?: Constants.EMPTY_STRING
            )
        )
    }
    return usersList
}

internal fun InsertUserRequest.toUserEntity(): UserEntity {
    return UserEntity(
        id = id,
        name = name,
        urlImage = urlImage,
        userName = userName,
    )
}

internal fun List<UserEntity>.toUserDomainModel(): List<UserDomainModel> {
    val usersList = mutableListOf<UserDomainModel>()
    repeat(count()) { userIndex ->
        usersList.add(
            UserDomainModel(
                id = this[userIndex].id,
                name = this[userIndex].name,
                urlImage = this[userIndex].urlImage,
                userName = this[userIndex].userName
            )
        )
    }
    return usersList
}