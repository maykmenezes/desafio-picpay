package com.maykmenezes.helper.extensionFunction

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.downloadImage(urlImage: String, placeholderImageResource: Int) {
    Glide
        .with(context)
        .load(urlImage)
        .placeholder(placeholderImageResource)
        .circleCrop()
        .into(this)
}