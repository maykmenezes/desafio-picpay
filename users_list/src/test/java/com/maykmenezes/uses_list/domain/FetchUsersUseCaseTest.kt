package com.maykmenezes.uses_list.domain

import com.maykmenezes.common.genericResult.GenericErrorResult
import com.maykmenezes.common.genericResult.GenericParamResult
import com.maykmenezes.common.genericResult.GenericResult
import com.maykmenezes.common.genericResult.GenericSuccessResult
import com.maykmenezes.users_list.data.repository.UsersRepository
import com.maykmenezes.users_list.domain.model.UserDomainModel
import com.maykmenezes.users_list.domain.useCase.FetchUsersUseCase
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

internal class FetchUsersUseCaseTest {

    private val repository = mockk<UsersRepository>(relaxed = true)
    private val useCase = FetchUsersUseCase(repository = repository)

    private val invalidParam = FetchUsersUseCaseTestHelper.invalidParam
    private val hasConnectionRequest = FetchUsersUseCaseTestHelper.hasConnectionRequest
    private val noConnectionRequest = FetchUsersUseCaseTestHelper.noConnectionRequest

    private val populatedSuccessResult = FetchUsersUseCaseTestHelper.populatedSuccessResult
    private val emptySuccessResult = FetchUsersUseCaseTestHelper.emptySuccessResult
    private val noConnectionErrorResult = FetchUsersUseCaseTestHelper.noConnectionErrorResult
    private val unknownErrorResult = FetchUsersUseCaseTestHelper.unknownErrorResult
    private val unauthorizedFoundErrorResult =
        FetchUsersUseCaseTestHelper.unauthorizedFoundErrorResult
    private val notFoundErrorResult = FetchUsersUseCaseTestHelper.notFoundErrorResult
    private val timeoutErrorResult = FetchUsersUseCaseTestHelper.timeoutErrorResult

    @Test
    fun `Given populated success result of repository, when execute then should call repository and return populated success result`() =
        runBlocking {
            // Given
            coEvery { repository.fetchUsers(hasConnectionRequest.hasConnection) } returns populatedSuccessResult
            // When
            val result = useCase.execute(hasConnectionRequest)
            // Then
            Assert.assertTrue(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Param)
            coVerify(exactly = 1) { repository.fetchUsers(hasConnectionRequest.hasConnection) }
            if (result is GenericResult.Success) {
                result.apply {
                    Assert.assertTrue(success is GenericSuccessResult.Populated)
                    Assert.assertFalse(success is GenericSuccessResult.Empty)
                    if (success is GenericSuccessResult.Populated) {
                        Assert.assertEquals(
                            (success as GenericSuccessResult.Populated<List<UserDomainModel>>).data,
                            (populatedSuccessResult.success as GenericSuccessResult.Populated<List<UserDomainModel>>).data
                        )
                    }
                }
            }
        }

    @Test
    fun `Given empty success result of repository, when execute then should call repository and return empty success result`() =
        runBlocking {
            // Given
            coEvery { repository.fetchUsers(hasConnectionRequest.hasConnection) } returns emptySuccessResult
            // When
            val result = useCase.execute(hasConnectionRequest)
            // Then
            Assert.assertTrue(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Param)
            coVerify(exactly = 1) { repository.fetchUsers(hasConnectionRequest.hasConnection) }
            if (result is GenericResult.Success) {
                Assert.assertTrue(result.success is GenericSuccessResult.Empty)
                Assert.assertFalse(result.success is GenericSuccessResult.Populated)
            }
        }

    @Test
    fun `When execute with invalid param then not should call repository and return invalid param error result`() =
        runBlocking {
            // Given
            coEvery { repository.fetchUsers(hasConnectionRequest.hasConnection) } returns noConnectionErrorResult
            // When
            val result = useCase.execute(invalidParam)
            // Then
            Assert.assertTrue(result is GenericResult.Param)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Error)
            coVerify(exactly = 0) { repository.fetchUsers(hasConnectionRequest.hasConnection) }
            if (result is GenericResult.Param) {
                Assert.assertTrue(result.param is GenericParamResult.Invalid)
            }
        }

    @Test
    fun `Given not connection error result of repository, when execute then should call repository and return not connection error result`() =
        runBlocking {
            // Given
            coEvery { repository.fetchUsers(noConnectionRequest.hasConnection) } returns noConnectionErrorResult
            // When
            val result = useCase.execute(noConnectionRequest)
            // Then
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            coVerify(exactly = 1) { repository.fetchUsers(noConnectionRequest.hasConnection) }
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.NoConnection)
                Assert.assertFalse(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.Timeout)
                Assert.assertFalse(result.error is GenericErrorResult.Unknown)
            }
        }

    @Test
    fun `Given unknown error result of repository, when execute then should call repository and return unknown error result`() =
        runBlocking {
            // Given
            coEvery { repository.fetchUsers(hasConnectionRequest.hasConnection) } returns unknownErrorResult
            // When
            val result = useCase.execute(hasConnectionRequest)
            // Then
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            coVerify(exactly = 1) { repository.fetchUsers(hasConnectionRequest.hasConnection) }
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.Unknown)
                Assert.assertFalse(result.error is GenericErrorResult.NoConnection)
                Assert.assertFalse(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.Timeout)
            }
        }

    @Test
    fun `Given unauthorized error result of repository, when execute then should call repository and return unauthorized error result`() =
        runBlocking {
            // Given
            coEvery { repository.fetchUsers(hasConnectionRequest.hasConnection) } returns unauthorizedFoundErrorResult
            // When
            val result = useCase.execute(hasConnectionRequest)
            // Then
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            coVerify(exactly = 1) { repository.fetchUsers(hasConnectionRequest.hasConnection) }
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.NoConnection)
                Assert.assertFalse(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Timeout)
                Assert.assertFalse(result.error is GenericErrorResult.Unknown)
            }
        }

    @Test
    fun `Given not found error result of repository, when execute then should call repository and return not found error result`() =
        runBlocking {
            // Given
            coEvery { repository.fetchUsers(hasConnectionRequest.hasConnection) } returns notFoundErrorResult
            // When
            val result = useCase.execute(hasConnectionRequest)
            // Then
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            coVerify(exactly = 1) { repository.fetchUsers(hasConnectionRequest.hasConnection) }
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.NoConnection)
                Assert.assertFalse(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.Timeout)
                Assert.assertFalse(result.error is GenericErrorResult.Unknown)
            }
        }

    @Test
    fun `Given time out error result of repository, when execute then should call repository and return time out error result`() =
        runBlocking {
            // Given
            coEvery { repository.fetchUsers(hasConnectionRequest.hasConnection) } returns timeoutErrorResult
            // When
            val result = useCase.execute(hasConnectionRequest)
            // Then
            Assert.assertTrue(result is GenericResult.Error)
            Assert.assertFalse(result is GenericResult.Success)
            Assert.assertFalse(result is GenericResult.Param)
            coVerify(exactly = 1) { repository.fetchUsers(hasConnectionRequest.hasConnection) }
            if (result is GenericResult.Error) {
                Assert.assertTrue(result.error is GenericErrorResult.Timeout)
                Assert.assertFalse(result.error is GenericErrorResult.NoConnection)
                Assert.assertFalse(result.error is GenericErrorResult.Unauthorized)
                Assert.assertFalse(result.error is GenericErrorResult.NotFound)
                Assert.assertFalse(result.error is GenericErrorResult.Unknown)
            }
        }
}