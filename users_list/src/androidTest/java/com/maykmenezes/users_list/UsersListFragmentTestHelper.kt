package com.maykmenezes.users_list

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.test.platform.app.InstrumentationRegistry
import com.maykmenezes.users_list.data.model.UserResponse
import com.maykmenezes.core.database.usersList.UserEntity

internal object UsersListFragmentTestHelper {

    val mockedLocalDataSourcePopulatedSuccessResponse = listOf(
        UserEntity(
            id = 1,
            name = "Robert",
            urlImage = "https://randomuser.me/api/portraits/men/1.jpg",
            userName = "@Robert"
        ),
        UserEntity(
            id = 2,
            name = "Leon",
            urlImage = "https://randomuser.me/api/portraits/men/2.jpg",
            userName = "@Leon"
        ),
        UserEntity(
            id = 3,
            name = "Lian",
            urlImage = "https://randomuser.me/api/portraits/men/3.jpg",
            userName = "@Lian"
        )
    )

    val mockedRemoteDataSourcePopulatedSuccessResponse = listOf(
        UserResponse(
            id = 1,
            name = "Robert",
            urlImage = "https://randomuser.me/api/portraits/men/1.jpg",
            userName = "@Robert"
        ),
        UserResponse(
            id = 2,
            name = "Leon",
            urlImage = "https://randomuser.me/api/portraits/men/2.jpg",
            userName = "@Leon"
        ),
        UserResponse(
            id = 3,
            name = "Lian",
            urlImage = "https://randomuser.me/api/portraits/men/3.jpg",
            userName = "@Lian"
        )
    )

    val mockedLocalDataSourceWithEmptySuccessResponse = listOf<UserEntity>()

    val mockedRemoteDataSourceWithEmptySuccessResponse = listOf<UserResponse>()

    fun hasConnection(): Boolean {
        val connectivityManager =
            InstrumentationRegistry.getInstrumentation().targetContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val networkCapabilities =
                connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            networkCapabilities.apply {
                return when {
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    else -> false
                }
            }
        } else {
            val activeNetworkInfo = connectivityManager.activeNetworkInfo ?: return false
            return activeNetworkInfo.isConnected
        }
    }
}