package com.maykmenezes.users_list.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.maykmenezes.users_list.databinding.UserItemLayoutBinding
import com.maykmenezes.users_list.presentation.model.UserUiModel

internal class UsersListAdapter(
    private val users: List<UserUiModel>
) : RecyclerView.Adapter<UsersListAdapterViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): UsersListAdapterViewHolder {
        val viewBinding =
            UserItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UsersListAdapterViewHolder(viewBinding)
    }

    override fun onBindViewHolder(holder: UsersListAdapterViewHolder, position: Int) {
        holder.bindView(users[position])
    }

    override fun getItemCount() = users.count()
}