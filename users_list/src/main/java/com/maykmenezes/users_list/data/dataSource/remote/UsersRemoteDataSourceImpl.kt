package com.maykmenezes.users_list.data.dataSource.remote

internal class UsersRemoteDataSourceImpl(
    private val api: UsersApi
) : UsersRemoteDataSource {

    override suspend fun fetchUsers() = api.fetchUsers()
}