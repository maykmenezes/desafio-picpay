package com.maykmenezes.users_list.presentation.mapper

import com.maykmenezes.users_list.domain.model.FetchUsersRequest
import com.maykmenezes.users_list.domain.model.InsertUserRequest
import com.maykmenezes.users_list.domain.model.UserDomainModel
import com.maykmenezes.users_list.presentation.model.UserUiModel

internal fun UserUiModel.toInsertUserRequest(): InsertUserRequest {
    return InsertUserRequest(
        id = id,
        name = name,
        urlImage = urlImage,
        userName = userName
    )
}

internal fun Boolean.toFetchUsersRequest(): FetchUsersRequest {
    return FetchUsersRequest(hasConnection = this)
}

internal fun List<UserDomainModel>.toUsersUiModel(): List<UserUiModel> {
    val usersList = mutableListOf<UserUiModel>()
    repeat(count()) { userIndex ->
        usersList.add(
            UserUiModel(
                id = this[userIndex].id,
                name = this[userIndex].name,
                urlImage = this[userIndex].urlImage,
                userName = this[userIndex].userName
            )
        )
    }
    return usersList
}