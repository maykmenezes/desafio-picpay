package com.maykmenezes.common.genericResult

sealed class GenericErrorResult<T> {
    class NotFound<T>(val throwable: Throwable) : GenericErrorResult<T>()
    class Unauthorized<T>(val throwable: Throwable) : GenericErrorResult<T>()
    class NoConnection<T> : GenericErrorResult<T>()
    class Timeout<T>(val throwable: Throwable) : GenericErrorResult<T>()
    class Unknown<T>(val throwable: Throwable) : GenericErrorResult<T>()
}