package com.maykmenezes.users_list.data.dataSource.remote

import com.maykmenezes.users_list.data.model.UserResponse
import retrofit2.http.GET

internal interface UsersApi {

    @GET(FETCH_USERS_END_POINT)
    suspend fun fetchUsers(): List<UserResponse>

    companion object {
        private const val FETCH_USERS_END_POINT = "users"
    }
}