package com.maykmenezes.common.genericResult

sealed class GenericParamResult<T> {
    class Invalid<T>(val messages: List<Int?>) : GenericParamResult<T>()
}