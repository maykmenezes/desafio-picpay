package com.maykmenezes.uses_list.data

import com.maykmenezes.core.database.usersList.UserEntity
import com.maykmenezes.users_list.data.model.UserResponse
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException

internal object UsersRepositoryTestHelper {

    val populatedLocalSuccessResult = listOf(
        UserEntity(
            id = 1,
            name = "Robert",
            urlImage = "https://randomuser.me/api/portraits/men/1.jpg",
            userName = "@Robert"
        ),
        UserEntity(
            id = 2,
            name = "Leon",
            urlImage = "https://randomuser.me/api/portraits/men/2.jpg",
            userName = "@Leon"
        ),
        UserEntity(
            id = 3,
            name = "Lian",
            urlImage = "https://randomuser.me/api/portraits/men/3.jpg",
            userName = "@Lian"
        )
    )

    val populatedRemoteSuccessResult = listOf(
        UserResponse(
            id = 1,
            name = "Robert",
            urlImage = "https://randomuser.me/api/portraits/men/1.jpg",
            userName = "@Robert"
        ),
        UserResponse(
            id = 2,
            name = "Leon",
            urlImage = "https://randomuser.me/api/portraits/men/2.jpg",
            userName = "@Leon"
        ),
        UserResponse(
            id = 3,
            name = "Lian",
            urlImage = "https://randomuser.me/api/portraits/men/3.jpg",
            userName = "@Lian"
        )
    )

    val emptyLocalSuccessResult = listOf<UserEntity>()
    val emptyRemoteSuccessResult = listOf<UserResponse>()

    fun notFoundError() = HttpException(getErrorResponseByCode(NOT_FOUND_ERROR))
    fun unauthorizedFoundError() = HttpException(getErrorResponseByCode(UNAUTHORIZED_ERROR))
    fun unknownError() = HttpException(getErrorResponseByCode(UNKNOWN_ERROR))
    fun timeoutError() = SocketTimeoutException()

    private fun getErrorResponseByCode(code: Int): Response<List<UserResponse>> {
        return Response.error(
            code, ResponseBody.create("application/json".toMediaTypeOrNull(), "")
        )
    }

    const val hasConnection = true

    private const val UNAUTHORIZED_ERROR = 401
    private const val NOT_FOUND_ERROR = 404
    private const val UNKNOWN_ERROR = 500
}