package com.maykmenezes.users_list.data.dataSource.local

import com.maykmenezes.core.database.usersList.UserEntity

internal interface UsersLocalDataSource {

    suspend fun insertUser(user: UserEntity)
    suspend fun fetchUsers(): List<UserEntity>
    suspend fun deleteAllUsers()
}