package com.maykmenezes.users_list.domain.di

import com.maykmenezes.users_list.data.dataSource.local.UsersLocalDataSource
import com.maykmenezes.users_list.data.dataSource.local.UsersLocalDataSourceImpl
import com.maykmenezes.users_list.data.dataSource.remote.UsersApi
import com.maykmenezes.users_list.data.dataSource.remote.UsersRemoteDataSource
import com.maykmenezes.users_list.data.dataSource.remote.UsersRemoteDataSourceImpl
import com.maykmenezes.users_list.data.repository.UsersRepository
import com.maykmenezes.users_list.data.repository.UsersRepositoryImpl
import com.maykmenezes.users_list.domain.useCase.DeleteAllUsersUseCase
import com.maykmenezes.users_list.domain.useCase.FetchUsersUseCase
import com.maykmenezes.users_list.domain.useCase.InsertUserUseCase
import com.maykmenezes.users_list.presentation.UsersListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object UsersListDiModule {

    val instance = module {

        single<UsersApi> {
            get<Retrofit>().create(UsersApi::class.java)
        }

        factory<UsersLocalDataSource> {
            UsersLocalDataSourceImpl(userDao = get())
        }

        factory<UsersRemoteDataSource> {
            UsersRemoteDataSourceImpl(api = get())
        }

        factory<UsersRepository> {
            UsersRepositoryImpl(localDataSource = get(), remoteDataSource = get())
        }

        factory {
            FetchUsersUseCase(repository = get())
        }

        factory {
            InsertUserUseCase(repository = get())
        }

        factory {
            DeleteAllUsersUseCase(repository = get())
        }

        viewModel {
            UsersListViewModel(
                fetchUsersUseCase = get(),
                insertUserUseCase = get(),
                deleteAllUsersUseCase = get()
            )
        }
    }
}