package com.maykmenezes.core.di

import androidx.room.Room
import com.maykmenezes.core.database.AppDatabase
import com.maykmenezes.core.network.Retrofit
import org.koin.dsl.module

object CoreDiModule {

    val instance = module {

        single {
            Retrofit().getRetrofit()
        }

        single {
            Room.databaseBuilder(get(), AppDatabase::class.java, DATABASE_NAME).build()
        }

        factory {
            get<AppDatabase>().userDao()
        }
    }

    private const val DATABASE_NAME = "app_database.db"
}